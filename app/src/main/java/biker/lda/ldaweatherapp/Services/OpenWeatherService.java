package biker.lda.ldaweatherapp.Services;

import android.support.annotation.NonNull;

import biker.lda.ldaweatherapp.Api.OpenweatherAPI;
import biker.lda.ldaweatherapp.Mvp.models.Weather;
import io.reactivex.Observable;

public class OpenWeatherService {

    private String APIKEY = "4357aec702381bfb4715904ab12d7cdc";
    private String lang = "ru";
    private String units = "metric";

    private OpenweatherAPI mOpenweatherAPI;

    public OpenWeatherService(@NonNull OpenweatherAPI openweatherAPI){
        mOpenweatherAPI = openweatherAPI;
    }

    @NonNull
    public Observable<Weather> getWeather(@NonNull String cityName){
        return mOpenweatherAPI.getWeather(cityName, lang, APIKEY, units);
    }

    @NonNull
    public Observable<Weather> getWeather(@NonNull Double lat, @NonNull Double lon){
        return mOpenweatherAPI.getWeather(lat, lon, lang, APIKEY, units);
    }

}
