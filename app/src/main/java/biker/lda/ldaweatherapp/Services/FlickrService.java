package biker.lda.ldaweatherapp.Services;


import android.support.annotation.NonNull;

import java.util.List;

import biker.lda.ldaweatherapp.Api.FlickrAPI;
import biker.lda.ldaweatherapp.Mvp.models.Photo;
import biker.lda.ldaweatherapp.Mvp.models.PhotoSize;
import biker.lda.ldaweatherapp.Mvp.models.Photos;
import io.reactivex.Observable;

public class FlickrService {

    private static String APIKEY = "ed8366d71277904d142af71fbaf45733";
    private static String APIKEYSECRET = "d29eec312861c9a5";
    private static String METHOD = "flickr.photos.search";
    private static String METHODSIZE = "flickr.photos.getSizes";
    private static String FORMAT = "json";
    private static Integer NOJSONCALLBACK = 1;
    private static Integer  PERPAGE = 1;

    private FlickrAPI mFlickrAPI;

    public FlickrService(@NonNull FlickrAPI flickrAPI){
        mFlickrAPI = flickrAPI;
    }

    @NonNull
    public Observable<Photos> getPhotos(@NonNull String text){
        return mFlickrAPI.searchPhoto(METHOD,APIKEY,text,PERPAGE,FORMAT,NOJSONCALLBACK);
    }

    @NonNull
    public Observable<List<PhotoSize>> getPhotoSize(@NonNull Photo photo){
        return mFlickrAPI.getPhotoSize(METHODSIZE, APIKEY, photo.getId(), FORMAT, NOJSONCALLBACK);
    }
}
