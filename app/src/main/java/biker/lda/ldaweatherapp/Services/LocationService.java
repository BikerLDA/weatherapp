package biker.lda.ldaweatherapp.Services;

import android.Manifest;

import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.util.Log;

import io.reactivex.Observable;

public class LocationService {
    private LocationManager mLocationManager;
    private Context context;

    public static Integer TAG_CODE_PERMISSION_LOCATION = 1;

    public LocationService(@NonNull Context context) {
        this.context = context;
        mLocationManager = (LocationManager) this.context.getSystemService(Context.LOCATION_SERVICE);
    }

    public Boolean checkEnableLocationService(){
        return mLocationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
    }

    public Observable<Location> getLastLocation() {
        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return Observable.error(new SecurityException("Location permission error"));
        }

        Observable<Location> networkObs = Observable.create(e -> mLocationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0,
                0, new LocationListener() {
                    @Override
                    public void onLocationChanged(Location location) {
                        Log.d("onLocationChanged", location.toString());
                        mLocationManager.removeUpdates(this);
                        e.onNext(location);
                        e.onComplete();
                    }

                    @Override
                    public void onStatusChanged(String provider, int status, Bundle extras) {

                    }

                    @Override
                    public void onProviderEnabled(String provider) {
                    }

                    @Override
                    public void onProviderDisabled(String provider) {

                    }
                }));
        Observable<Location> gpsObs = Observable.create(e -> mLocationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0,
                0, new LocationListener() {
                    @Override
                    public void onLocationChanged(Location location) {
                        Log.d("onLocationChanged", location.toString());
                        mLocationManager.removeUpdates(this);
                        e.onNext(location);
                        e.onComplete();
                    }

                    @Override
                    public void onStatusChanged(String provider, int status, Bundle extras) {

                    }

                    @Override
                    public void onProviderEnabled(String provider) {
                    }

                    @Override
                    public void onProviderDisabled(String provider) {

                    }
                }));

        return Observable.merge(networkObs, gpsObs).take(1);
    }

}

