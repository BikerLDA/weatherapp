package biker.lda.ldaweatherapp.Api;

import java.util.List;

import biker.lda.ldaweatherapp.Mvp.models.PhotoSize;
import biker.lda.ldaweatherapp.Mvp.models.Photos;
import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface FlickrAPI {
    @GET("/services/rest")
    Observable<Photos> searchPhoto(@Query("method") String method,
                                   @Query("api_key") String apiKey,
                                   @Query("text") String text,
                                   @Query("per_page") Integer perPage,
                                   @Query("format") String format,
                                   @Query("nojsoncallback") Integer nojsoncallback);

    @GET("/services/rest")
    Observable<List<PhotoSize>> getPhotoSize(@Query("method") String method,
                                             @Query("api_key") String apiKey,
                                             @Query("photo_id") String photoId,
                                             @Query("format") String format,
                                             @Query("nojsoncallback") Integer nojsoncallback);
}
