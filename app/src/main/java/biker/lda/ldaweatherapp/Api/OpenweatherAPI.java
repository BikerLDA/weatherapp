package biker.lda.ldaweatherapp.Api;


import biker.lda.ldaweatherapp.Mvp.models.Weather;
import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface OpenweatherAPI {

    /**
     *
     * @param cityName city name
     * @param lang language
     * @param appid API key
     * @return current weather by city name
     */
    @GET("/data/2.5/weather")
    Observable<Weather> getWeather(@Query("q") String cityName, @Query("lang") String lang , @Query("appid") String appid, @Query("units") String units);

    /**
     *
     * @param lat lat
     * @param lon   lon
     * @param lang  lang
     * @param appid API key
     * @return current weather by geographic coordinates
     */
    @GET("/data/2.5/weather")
    Observable<Weather> getWeather(@Query("lat") Double lat, @Query("lon") Double lon, @Query("lang") String lang , @Query("appid") String appid, @Query("units") String units);
}
