package biker.lda.ldaweatherapp.di.modules;

import javax.inject.Singleton;

import biker.lda.ldaweatherapp.Api.FlickrAPI;
import biker.lda.ldaweatherapp.Services.FlickrService;
import dagger.Module;
import dagger.Provides;

@Module(includes = {FlickrApiModule.class})
public class FlickrModule {
    @Provides
    @Singleton
    public FlickrService provideOpenWeatherService(FlickrAPI flickrAPI){
        return new FlickrService(flickrAPI);
    }
}
