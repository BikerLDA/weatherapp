package biker.lda.ldaweatherapp.di.modules;

import javax.inject.Named;
import javax.inject.Singleton;

import biker.lda.ldaweatherapp.Api.FlickrAPI;
import dagger.Module;
import dagger.Provides;
import retrofit2.Retrofit;

@Module(includes = {RetrofitFlickrAPIModule.class})
public class FlickrApiModule {
    @Provides
    @Singleton
    public FlickrAPI provideFlickrAPI(@Named("Flickr") Retrofit retrofit){
        return retrofit.create(FlickrAPI.class);
    }
}
