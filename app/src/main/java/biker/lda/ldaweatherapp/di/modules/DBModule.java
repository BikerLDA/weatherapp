package biker.lda.ldaweatherapp.di.modules;

import android.content.Context;
import android.database.sqlite.SQLiteOpenHelper;
import android.support.annotation.NonNull;

import com.pushtorefresh.storio.sqlite.StorIOSQLite;
import com.pushtorefresh.storio.sqlite.impl.DefaultStorIOSQLite;

import javax.inject.Singleton;

import biker.lda.ldaweatherapp.Mvp.models.City;
import biker.lda.ldaweatherapp.Mvp.models.CitySQLiteTypeMapping;
import biker.lda.ldaweatherapp.db.DbOpenHelper;
import dagger.Module;
import dagger.Provides;

@Module
public class DBModule {

    @Provides
    @NonNull
    @Singleton
    StorIOSQLite provideStorIOSQLite(@NonNull SQLiteOpenHelper sqLiteOpenHelper) {
        return DefaultStorIOSQLite.builder()
                .sqliteOpenHelper(sqLiteOpenHelper)
                .addTypeMapping(City.class, new CitySQLiteTypeMapping())
                .build();
    }

    @Provides
    @NonNull
    @Singleton
    SQLiteOpenHelper provideSQLiteOpenHelper(@NonNull Context context) {
        return new DbOpenHelper(context);
    }
}
