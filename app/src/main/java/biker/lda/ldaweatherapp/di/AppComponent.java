package biker.lda.ldaweatherapp.di;

import android.content.Context;
import javax.inject.Singleton;

import biker.lda.ldaweatherapp.Mvp.presenters.HomePresenter;
import biker.lda.ldaweatherapp.Services.FlickrService;
import biker.lda.ldaweatherapp.Services.LocationService;
import biker.lda.ldaweatherapp.Services.OpenWeatherService;
import biker.lda.ldaweatherapp.di.modules.CityRepositoryModule;
import biker.lda.ldaweatherapp.di.modules.ContextModule;
import biker.lda.ldaweatherapp.di.modules.FlickrModule;
import biker.lda.ldaweatherapp.di.modules.LocationModule;
import biker.lda.ldaweatherapp.di.modules.OpenweatherModule;
import biker.lda.ldaweatherapp.providers.City.CityRepository;
import dagger.Component;

@Singleton
@Component(modules = {ContextModule.class, CityRepositoryModule.class, OpenweatherModule.class, LocationModule.class, FlickrModule.class})
public interface AppComponent {
    CityRepository getCityRepository();
    OpenWeatherService getOpenWeatherService();
    LocationService locationService();
    FlickrService getFlickrService();

    Context getContext();

    void inject(HomePresenter homePresenter);
}
