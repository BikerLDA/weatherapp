package biker.lda.ldaweatherapp.di.modules;

import javax.inject.Singleton;

import biker.lda.ldaweatherapp.Api.OpenweatherAPI;
import dagger.Module;
import dagger.Provides;
import retrofit2.Retrofit;

@Module(includes = {RetrofitOpenWeatherModule.class})

public class OpenWeatherApiModule {
    @Provides
    @Singleton
    public OpenweatherAPI provideOpenweatherAPI(Retrofit retrofit){
        return retrofit.create(OpenweatherAPI.class);
    }
}
