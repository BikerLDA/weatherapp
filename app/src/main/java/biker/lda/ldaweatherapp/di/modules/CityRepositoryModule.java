package biker.lda.ldaweatherapp.di.modules;

import android.support.annotation.NonNull;

import com.pushtorefresh.storio.sqlite.StorIOSQLite;

import javax.inject.Singleton;

import biker.lda.ldaweatherapp.providers.City.CityRepository;
import dagger.Module;
import dagger.Provides;

@Module(includes = DBModule.class)
public class CityRepositoryModule {

    @Provides
    @NonNull
    @Singleton
    CityRepository getCityRepository(StorIOSQLite storIOSQLite){
        return new CityRepository(storIOSQLite);
    }
}
