package biker.lda.ldaweatherapp.di.modules;

import javax.inject.Singleton;

import biker.lda.ldaweatherapp.Api.OpenweatherAPI;
import biker.lda.ldaweatherapp.Services.OpenWeatherService;
import dagger.Module;
import dagger.Provides;

@Module(includes = {OpenWeatherApiModule.class})
public class OpenweatherModule {
    @Provides
    @Singleton
    public OpenWeatherService provideOpenWeatherService(OpenweatherAPI openweatherAPI){
        return new OpenWeatherService(openweatherAPI);
    }
}
