package biker.lda.ldaweatherapp.di.modules;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;

import java.lang.reflect.Type;

import javax.inject.Singleton;

import biker.lda.ldaweatherapp.Mvp.models.City;
import biker.lda.ldaweatherapp.Mvp.models.Weather;
import dagger.Module;
import dagger.Provides;
import io.reactivex.schedulers.Schedulers;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Converter;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

@Module
public class RetrofitOpenWeatherModule {
    @Provides
    @Singleton
    Retrofit provideRetrofitOpenWeather(Retrofit.Builder builder) {
        return builder.baseUrl("http://api.openweathermap.org").build();
    }

    @Provides
    @Singleton
    Retrofit.Builder provideRetrofitOpenWeatherBuilder(Converter.Factory converterFactory) {

        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BASIC);
        OkHttpClient client = new OkHttpClient.Builder()
                .addInterceptor(logging)
                .build();

        return new Retrofit.Builder()
                .addCallAdapterFactory(RxJava2CallAdapterFactory.createWithScheduler(Schedulers.io()))
                .addConverterFactory(converterFactory)
                .client(client);
    }

    @Provides
    @Singleton
    Converter.Factory provideConverterFactory(Gson gson) {
        return GsonConverterFactory.create(gson);
    }

    @Provides
    @Singleton
    Gson provideGson() {
        return new GsonBuilder()
                .setPrettyPrinting()
                .registerTypeAdapter(Weather.class, new WeatherDeserializer())
                .serializeNulls()
                .create();
    }

    private class WeatherDeserializer implements JsonDeserializer<Weather> {

        @Override
        public Weather deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
            final JsonObject jsonObject = json.getAsJsonObject();
            if (jsonObject == null) {
                throw new JsonParseException("JsonObject is empty");
            }

            String type;
            String typeDescription;
            String typeIcon;
            String iconUrl;
            if (!jsonObject.get("weather").isJsonNull() && jsonObject.get("weather").isJsonArray()) {
                JsonArray weatherArray = jsonObject.get("weather").getAsJsonArray();
                if(weatherArray.size() > 0 && weatherArray.get(0).isJsonObject()){
                    JsonObject weatherJsonObject = weatherArray.get(0).getAsJsonObject();

                    if(!weatherJsonObject.get("main").isJsonNull()){
                        type = weatherJsonObject.get("main").getAsString();
                    }
                    else{
                        throw new JsonParseException("Object weather main is empty");
                    }
                    if(!weatherJsonObject.get("description").isJsonNull()){
                        typeDescription = weatherJsonObject.get("description").getAsString();
                    }
                    else{
                        throw new JsonParseException("Object weather main is empty");
                    }
                    if(!weatherJsonObject.get("icon").isJsonNull()){
                        typeIcon = weatherJsonObject.get("icon").getAsString();
                        iconUrl = "http://openweathermap.org/img/w/" + typeIcon + ".png";
                    }
                    else{
                        throw new JsonParseException("Object weather icon is empty");
                    }
                }
                else{
                    throw new JsonParseException("Object weather is empty");
                }
            } else {
                throw new JsonParseException("Object weather is empty");
            }

            Float windSpeed;

            if (!jsonObject.get("wind").isJsonNull()
                    && jsonObject.get("wind").isJsonObject()
                    && !jsonObject.get("wind").getAsJsonObject().get("speed").isJsonNull()) {
                windSpeed = jsonObject.get("wind").getAsJsonObject().get("speed").getAsFloat();
            }else{
                throw new JsonParseException("Object wind is empty");
            }

            Long date;
            if (!jsonObject.get("dt").isJsonNull()){
                date = jsonObject.get("dt").getAsLong();
            }else{
                throw new JsonParseException("Object dt is empty");
            }

            City city;
            if (!jsonObject.get("name").isJsonNull()){
                city = City.newCity(jsonObject.get("name").getAsString());
            }else{
                throw new JsonParseException("Object name is empty");
            }

            Float temp;
            if (!jsonObject.get("main").isJsonNull()
                    && jsonObject.get("main").isJsonObject()
                    && !jsonObject.get("main").getAsJsonObject().get("temp").isJsonNull()) {
                temp = jsonObject.get("main").getAsJsonObject().get("temp").getAsFloat();
            }else{
                throw new JsonParseException("Object main is empty");
            }

            return Weather.newWeather(type,typeDescription,typeIcon,iconUrl,temp,windSpeed,city,date);
        }
    }
}
