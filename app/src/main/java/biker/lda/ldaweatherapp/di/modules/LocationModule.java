package biker.lda.ldaweatherapp.di.modules;

import android.content.Context;
import android.support.annotation.NonNull;

import javax.inject.Singleton;

import biker.lda.ldaweatherapp.Services.LocationService;
import dagger.Module;
import dagger.Provides;

@Module
public class LocationModule {
    @Provides
    @NonNull
    @Singleton
    LocationService provideLocationService(@NonNull Context context){
        return new LocationService(context);
    }
}
