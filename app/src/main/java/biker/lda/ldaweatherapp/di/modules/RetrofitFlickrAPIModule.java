package biker.lda.ldaweatherapp.di.modules;


import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.List;

import javax.inject.Named;
import javax.inject.Singleton;

import biker.lda.ldaweatherapp.Mvp.models.PhotoSize;
import biker.lda.ldaweatherapp.Mvp.models.Photos;
import dagger.Module;
import dagger.Provides;
import io.reactivex.schedulers.Schedulers;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Converter;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

@Module
public class RetrofitFlickrAPIModule {
    @Provides
    @Singleton
    @Named("Flickr")
    Retrofit provideRetrofitFlickr(@Named("Flickr") Retrofit.Builder builder) {

        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder()
                .addInterceptor(logging)
                .build();


        return builder.baseUrl("https://api.flickr.com").client(client).build();
    }

    @Provides
    @Singleton
    @Named("Flickr")
    Retrofit.Builder provideRetrofitFlickrBuilder(@Named("Flickr") Converter.Factory converterFactory) {

        return new Retrofit.Builder()
                .addCallAdapterFactory(RxJava2CallAdapterFactory.createWithScheduler(Schedulers.io()))
                .addConverterFactory(converterFactory);

    }

    @Provides
    @Singleton
    @Named("Flickr")
    Converter.Factory provideConverterFactory(@Named("Flickr") Gson gson) {
        return GsonConverterFactory.create(gson);
    }

    @Provides
    @Singleton
    @Named("Flickr")
    Gson provideGson() {
        Type listType = new TypeToken<List<PhotoSize>>(){}.getType();
        return new GsonBuilder()
                .setPrettyPrinting()
                .registerTypeAdapter(Photos.class, new PhotosDeserializer())
                .registerTypeAdapter(listType, new PhotoSizeDeserializer())
                .serializeNulls()
                .create();
    }

    private class PhotosDeserializer implements JsonDeserializer<Photos>{

        @Override
        public Photos deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
            final JsonObject jsonObject = json.getAsJsonObject();
            if (jsonObject == null) {
                throw new JsonParseException("JsonObject is empty");
            }
            Photos photos;
            if (!jsonObject.get("photos").isJsonNull() && jsonObject.get("photos").isJsonObject()) {
                photos = new Gson().fromJson(jsonObject.get("photos").getAsJsonObject(), Photos.class);
            }
            else{
                throw new JsonParseException("photos is empty");
            }
            return photos;
        }
    }

    private class PhotoSizeDeserializer implements JsonDeserializer<List<PhotoSize>>{

        @Override
        public List<PhotoSize> deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
            final JsonObject jsonObject = json.getAsJsonObject();
            if (jsonObject == null) {
                throw new JsonParseException("JsonObject is empty");
            }
            Type listType = new TypeToken<List<PhotoSize>>(){}.getType();
            List<PhotoSize> photoSizes;
            if (!jsonObject.get("sizes").isJsonNull() && jsonObject.get("sizes").isJsonObject()) {
                JsonObject sizes = jsonObject.get("sizes").getAsJsonObject();
                if(!sizes.get("size").isJsonNull() && sizes.get("size").isJsonArray()) {
                    photoSizes = new Gson().fromJson(sizes.get("size").getAsJsonArray(), listType);
                }else{
                    throw new JsonParseException("size is empty");
                }
            }
            else{
                throw new JsonParseException("sizes is empty");
            }
            return photoSizes;
        }
    }
}
