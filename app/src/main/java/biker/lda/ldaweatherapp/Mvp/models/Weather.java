package biker.lda.ldaweatherapp.Mvp.models;

import android.support.annotation.NonNull;

public class Weather {

    private String type;
    private String typeDescription;
    private String typeIcon;
    private String iconUrl;
    private Float temp;
    private Float windSpeed;
    private City city;
    private Long date;

    private Weather(){}

    private Weather(@NonNull String type, @NonNull String typeDescription, @NonNull String typeIcon, @NonNull String iconUrl, @NonNull Float temp, @NonNull Float windSpeed, @NonNull City city, @NonNull Long date){
        this.type = type;
        this.typeDescription = typeDescription;
        this.typeIcon = typeIcon;
        this.iconUrl = iconUrl;
        this.temp = temp;
        this.windSpeed = windSpeed;
        this.city = city;
        this.date = date;
    }

    public static Weather newWeather(@NonNull String type, @NonNull String typeDescription, @NonNull String typeIcon, @NonNull String iconUrl, @NonNull Float temp, @NonNull Float windSpeed, @NonNull City city, @NonNull Long date){
        return new Weather(type, typeDescription, typeIcon, iconUrl, temp, windSpeed, city, date);
    }

    public String getIconUrl() {
        return iconUrl;
    }

    public void setIconUrl(String iconUrl) {
        this.iconUrl = iconUrl;
    }

    public Long getDate() {
        return date;
    }

    public void setDate(Long date) {
        this.date = date;
    }

    public String getType() {
        return type;
    }

    public City getCity() {
        return city;
    }

    public void setCity(City city) {
        this.city = city;
    }

    public String getTypeDiscription() {
        return typeDescription;
    }

    public String getTypeIcon() {
        return typeIcon;
    }

    public Float getTemp() {
        return temp;
    }

    public Float getWindSpeed() {
        return windSpeed;
    }

    public void setType(String type) {
        this.type = type;
    }

    public void setTypeDiscription(String typeDiscription) {
        this.typeDescription = typeDiscription;
    }

    public void setTypeIcon(String typeIcon) {
        this.typeIcon = typeIcon;
    }

    public void setTemp(Float temp) {
        this.temp = temp;
    }

    public void setWindSpeed(Float windSpeed) {
        this.windSpeed = windSpeed;
    }
}
