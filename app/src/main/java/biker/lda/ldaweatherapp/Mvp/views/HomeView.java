package biker.lda.ldaweatherapp.Mvp.views;

import com.arellomobile.mvp.MvpView;
import com.arellomobile.mvp.viewstate.strategy.AddToEndSingleStrategy;
import com.arellomobile.mvp.viewstate.strategy.SkipStrategy;
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType;

import java.util.List;

import biker.lda.ldaweatherapp.Mvp.models.City;
import biker.lda.ldaweatherapp.Mvp.models.PhotoSize;
import biker.lda.ldaweatherapp.Mvp.models.Weather;

@StateStrategyType(AddToEndSingleStrategy.class)
public interface HomeView extends MvpView {
    void setWeather(Weather weather);
    void setCityList(List<City> cityList);
    void addCityInList(City city);
    void hideCityList();
    void showCityList();
    void showError(String message);
    void hideError();
    void onStartLoading();
    void onFinishLoading();
    void showRefreshing();
    void hideRefreshing();
    void onPermissionError();
    @StateStrategyType(SkipStrategy.class)
    void checkLocationManager();
    void setBackgroundImage(PhotoSize photoSize);
}
