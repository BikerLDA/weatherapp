package biker.lda.ldaweatherapp.Mvp.presenters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.util.Log;

import com.arellomobile.mvp.InjectViewState;

import javax.inject.Inject;

import biker.lda.ldaweatherapp.Mvp.models.City;
import biker.lda.ldaweatherapp.Mvp.models.PhotoSize;
import biker.lda.ldaweatherapp.Mvp.models.Weather;
import biker.lda.ldaweatherapp.Mvp.views.HomeView;
import biker.lda.ldaweatherapp.Services.FlickrService;
import biker.lda.ldaweatherapp.Services.LocationService;
import biker.lda.ldaweatherapp.Services.OpenWeatherService;
import biker.lda.ldaweatherapp.app.WeatherApp;
import biker.lda.ldaweatherapp.providers.City.CityRepository;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

@InjectViewState
public class HomePresenter extends BasePresenter<HomeView> {

    @Inject
    OpenWeatherService mOpenWeatherService;

    @Inject
    LocationService mLocationService;

    @Inject
    FlickrService mFlickrService;

    @Inject
    Context context;

    @Inject
    CityRepository mCityRepository;

    private Boolean loading = false;
    private City currentCity;

    public HomePresenter() {
        WeatherApp.getAppComponent().inject(this);
    }

    @Override
    protected void onFirstViewAttach() {
        super.onFirstViewAttach();
        getLocation();
        setCityList();
    }

    public void loadWeather(String cityName) {
        stopLoading();
        startRefresh();
        Disposable disposable = mOpenWeatherService.getWeather(cityName)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .flatMap(this::saveCityFromWeatherReq)
                .subscribe(data -> {
                    stopRefresh();
                    if(data != null) {
                        loadBackground(data.getCity());
                        getViewState().setWeather(data);
                    }
                }, error -> {
                    getViewState().showError(error.getMessage());
                    stopLoading();
                    stopRefresh();
                    Log.e("loadWeather", error.getMessage());
                });

        unsubscribeOnDestroy(disposable);
    }

    public void loadWeather(Double lat, Double lon) {
        stopLoading();
        startRefresh();
        Disposable disposable = mOpenWeatherService.getWeather(lat, lon)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .flatMap(this::saveCityFromWeatherReq)
                .subscribe(data -> {
                    stopRefresh();
                    if(data != null) {
                        if(currentCity != null) {
                            currentCity.setLon(lon);
                            currentCity.setLat(lat);
                        }
                        getViewState().setWeather(data);
                        loadBackground(data.getCity());
                    }
                }, error -> {
                    getViewState().showError(error.getMessage());
                    stopLoading();
                    stopRefresh();
                    Log.e("loadWeather", error.getMessage());
                });

        unsubscribeOnDestroy(disposable);
    }

    public void getLocation() {
        stopLoading();
        startRefresh();
        getViewState().onStartLoading();
        chechLocationManager();
        Disposable disp = mLocationService.getLastLocation()
                .subscribeOn(AndroidSchedulers.mainThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        location -> loadWeather(location.getLatitude(), location.getLongitude()),
                        exception -> {
                            if (exception instanceof SecurityException) {
                                getViewState().onPermissionError();
                            } else {
                                getViewState().showError(exception.getMessage());
                                stopLoading();
                                stopRefresh();
                                Log.e("Error", exception.getMessage());
                            }
                        }
                );
        unsubscribeOnDestroy(disp);
    }

    public void loadBackground(@NonNull City city) {
        mFlickrService.getPhotos(city.getName())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .flatMap(photos -> {
                    if (photos != null && photos.getPhoto() != null && photos.getPhoto().size() > 0) {
                        return mFlickrService.getPhotoSize(photos.getPhoto().get(0));
                    } else {
                        return Observable.error(new Throwable("Error"));
                    }
                })
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(data -> {
                    if(data != null && data.size() > 0){
                        for (PhotoSize photoSize: data) {
                            if(photoSize.getLabel() != null && photoSize.getLabel().equals("Small")){
                                getViewState().setBackgroundImage(photoSize);
                                break;
                            }
                        }
                    }
                }, error -> Log.e("Err", error.getMessage()));
    }

    public void stopLoading(){
        stopRefresh();
        stopAllDisposable();
    }

    public void refrash(){
        stopLoading();
        if(currentCity != null){
            if(currentCity.getLat() != null && currentCity.getLon() != null){
                loadWeather(currentCity.getLat(),currentCity.getLon());
            }else {
                loadWeather(currentCity.getName());
            }
        }else{
            getLocation();
        }
    }

    private void setCityList() {

        Disposable disp = mCityRepository.getCities()
                .subscribeOn(AndroidSchedulers.mainThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(cityList -> {
                            getViewState().setCityList(cityList);
                        }
                        , error -> Log.e("Err", error.getMessage()));
        unsubscribeOnDestroy(disp);
    }

    private void startRefresh(){
        if(!loading){
            loading = true;
            getViewState().showRefreshing();
        }
    }

    private void stopRefresh(){
        if(loading){
            loading = false;
            getViewState().hideRefreshing();
        }
    }

    private void chechLocationManager(){
        if(!mLocationService.checkEnableLocationService()){
            getViewState().checkLocationManager();
        }
    }

    private Observable<Weather> saveCityFromWeatherReq(Weather weather) {
        currentCity = weather.getCity();
        return mCityRepository.isUniq(currentCity).flatMap(isUniq -> {
            if (isUniq) {
                getViewState().addCityInList(currentCity);
                return mCityRepository.saveCity(currentCity);
            } else {
                return Observable.just(true);
            }
        }).flatMap(data -> Observable.just(weather));
    }
}
