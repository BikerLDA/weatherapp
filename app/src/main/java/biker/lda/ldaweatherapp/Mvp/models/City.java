package biker.lda.ldaweatherapp.Mvp.models;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.pushtorefresh.storio.sqlite.annotations.StorIOSQLiteColumn;
import com.pushtorefresh.storio.sqlite.annotations.StorIOSQLiteType;

import biker.lda.ldaweatherapp.db.tables.CityTable;

@StorIOSQLiteType(table = "cities")
public class City {
    @Nullable
    @StorIOSQLiteColumn(name = CityTable.COLUMN_ID, key = true)
    Long id;

    @NonNull
    @StorIOSQLiteColumn(name = CityTable.COLUMN_NAME, key = true)
    String name = "Empty";

    @Nullable
    @StorIOSQLiteColumn(name = CityTable.COLUMN_LON, key = true)
    Double lon;

    @Nullable
    @StorIOSQLiteColumn(name = CityTable.COLUMN_LAT, key = true)
    Double lat;

    @Nullable
    public Long getId() {
        return id;
    }

    public void setId(@Nullable Long id) {
        this.id = id;
    }

    @Nullable
    public Double getLon() {
        return lon;
    }

    public void setLon(@Nullable Double lon) {
        this.lon = lon;
    }

    @Nullable
    public Double getLat() {
        return lat;
    }

    public void setLat(@Nullable Double lat) {
        this.lat = lat;
    }

    City(){}

    private City(@Nullable Long id, @NonNull String name){
        this.id = id;
        this.name = name;
    }

    @NonNull
    public static City newCity(@NonNull String name){
        return new City(null, name);
    }

    @NonNull
    public String getName() {
        return name;
    }

    public void setName(@NonNull String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return this.name;
    }
}
