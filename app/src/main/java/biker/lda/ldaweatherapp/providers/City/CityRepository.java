package biker.lda.ldaweatherapp.providers.City;

import android.support.annotation.NonNull;

import com.pushtorefresh.storio.sqlite.StorIOSQLite;
import com.pushtorefresh.storio.sqlite.queries.Query;

import java.util.List;

import biker.lda.ldaweatherapp.Mvp.models.City;
import biker.lda.ldaweatherapp.db.tables.CityTable;
import io.reactivex.Observable;

public class CityRepository implements CityDataSource {

    private StorIOSQLite mStorIOSQLite;

    public CityRepository(StorIOSQLite storIOSQLite) {
        mStorIOSQLite = storIOSQLite;
    }

    @NonNull
    @Override
    public Observable<List<City>> getCities() {

        return Observable.just(mStorIOSQLite
                .get()
                .listOfObjects(City.class)
                .withQuery(Query.builder()
                        .table(CityTable.TABLE)
                        .build())
                .prepare().executeAsBlocking()
        );
    }

    @NonNull
    @Override
    public Observable<City> getCity(@NonNull String name) {
        City city = mStorIOSQLite.get()
                .object(City.class)
                .withQuery(
                        Query.builder()
                                .table(CityTable.TABLE)
                                .where("name = ?")
                                .whereArgs(name)
                                .build()
                ).prepare().executeAsBlocking();
        if(city != null) {
            return Observable.just(city);
        }else{
            return Observable.error(new Throwable("No Data"));
        }
    }

    @NonNull
    @Override
    public Observable<City> saveCity(@NonNull City city) {
        mStorIOSQLite.put()
                .object(city)
                .prepare()
                .executeAsBlocking();
        return Observable.just(city);
    }
    @NonNull
    @Override
    public Observable<City> saveUniqCity(@NonNull City city) {

        City oldCity = mStorIOSQLite.get()
                .object(City.class)
                .withQuery(
                        Query.builder()
                                .table(CityTable.TABLE)
                                .where("name = ?")
                                .whereArgs(city.getName())
                                .build()
                ).prepare().executeAsBlocking();

        if (oldCity == null) {
            mStorIOSQLite.put()
                    .object(city)
                    .prepare()
                    .executeAsBlocking();
        }
        return Observable.just(city);
    }

    @NonNull
    @Override
    public Observable<Boolean> isUniq(@NonNull City city) {
        City oldCity = mStorIOSQLite.get()
                .object(City.class)
                .withQuery(
                        Query.builder()
                                .table(CityTable.TABLE)
                                .where("name = ?")
                                .whereArgs(city.getName())
                                .build()
                ).prepare().executeAsBlocking();
        return Observable.just(oldCity == null);
    }


}
