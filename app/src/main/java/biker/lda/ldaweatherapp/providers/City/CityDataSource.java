package biker.lda.ldaweatherapp.providers.City;

import android.support.annotation.NonNull;

import java.util.List;

import biker.lda.ldaweatherapp.Mvp.models.City;
import io.reactivex.Observable;

public interface CityDataSource {

    @NonNull
    Observable<List<City>> getCities();

    @NonNull
    Observable<City> getCity(@NonNull String name);

    @NonNull
    Observable<City> saveCity(@NonNull City city);

    @NonNull
    Observable<City> saveUniqCity(@NonNull City city);

    @NonNull
    Observable<Boolean> isUniq(@NonNull City city);
}
