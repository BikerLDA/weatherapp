package biker.lda.ldaweatherapp.app;

import android.app.Application;

import biker.lda.ldaweatherapp.di.AppComponent;
import biker.lda.ldaweatherapp.di.DaggerAppComponent;
import biker.lda.ldaweatherapp.di.modules.ContextModule;
import biker.lda.ldaweatherapp.di.modules.DBModule;

public class WeatherApp extends Application {
    private static AppComponent sAppComponent;

    @Override
    public void onCreate() {
        super.onCreate();

        sAppComponent = DaggerAppComponent.builder()
                .contextModule(new ContextModule(this))
                .dBModule(new DBModule())
                .build();
    }

    public static AppComponent getAppComponent() {
        return sAppComponent;
    }
}
