package biker.lda.ldaweatherapp.ui.activity;

import android.Manifest;
import android.content.Intent;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.ListViewCompat;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.arellomobile.mvp.MvpAppCompatActivity;
import com.arellomobile.mvp.presenter.InjectPresenter;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import biker.lda.ldaweatherapp.Mvp.models.City;
import biker.lda.ldaweatherapp.Mvp.models.PhotoSize;
import biker.lda.ldaweatherapp.Mvp.models.Weather;
import biker.lda.ldaweatherapp.Mvp.presenters.HomePresenter;
import biker.lda.ldaweatherapp.Mvp.views.HomeView;
import biker.lda.ldaweatherapp.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends MvpAppCompatActivity implements HomeView, SwipeRefreshLayout.OnRefreshListener,SearchView.OnQueryTextListener, SearchView.OnFocusChangeListener {

    private static final int TAG_CODE_PERMISSION_LOCATION = 1;

    @InjectPresenter
    HomePresenter mHomePresenter;

    @BindView(R.id.activity_home_swipe_refresh_layout)
    SwipeRefreshLayout mSwipeRefreshLayout;

    @BindView(R.id.weatherTemp)
    TextView mWeatherTemp;

    @BindView(R.id.weatherImage)
    ImageView mWeatherImage;

    @BindView(R.id.windSpeedText)
    TextView mWindSpeedText;

    @BindView(R.id.backgroundPhoto)
    ImageView mBackgroundPhoto;

    @BindView(R.id.cityNameText)
    TextView mCityNameText;

    @BindView(R.id.mainToolbar)
    Toolbar mToolbar;

    @BindView(R.id.citySelectLayout)
    RelativeLayout mCitySelectLayout;

    @BindView(R.id.cityListView)
    ListViewCompat mCityListView;

    ArrayAdapter<City> arrayAdapter;

    SearchView searchView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        mSwipeRefreshLayout.setOnRefreshListener(this);

        arrayAdapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, new ArrayList<>());
        mCityListView.setAdapter(arrayAdapter);
        setSupportActionBar(mToolbar);

        mCityListView.setOnItemClickListener((parent, view, position, id) -> {
            if(arrayAdapter != null && arrayAdapter.getItem(position)!= null){
                City city = arrayAdapter.getItem(position);
                if(city != null && city.getLon() != null && city.getLat() != null){
                    mHomePresenter.loadWeather(city.getLat(), city.getLon());
                }else if(city != null){
                    mHomePresenter.loadWeather(city.getName());
                }
                hideCityList();
                if(searchView != null){
                    searchView.setIconified(true);
                }
            }
        });
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        menu.clear();
        getMenuInflater().inflate(R.menu.main_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        if (menu.findItem(R.id.search_view) != null) {
            searchView = (SearchView) menu.findItem(R.id.search_view).getActionView();
            searchView.setOnQueryTextListener(this);
            searchView.setOnQueryTextFocusChangeListener(this);
        }
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public void onPermissionError() {
        ActivityCompat.requestPermissions(this, new String[]{
                Manifest.permission.ACCESS_FINE_LOCATION,
                Manifest.permission.ACCESS_COARSE_LOCATION}, TAG_CODE_PERMISSION_LOCATION);
    }

    @Override
    public void checkLocationManager() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Location Manager");
        builder.setMessage("Would you like to enable GPS?");
        builder.setPositiveButton("Yes", (dialog, which) -> {
            Intent i = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
            startActivity(i);
        });
        builder.setNegativeButton("No", (dialog, which) -> mHomePresenter.stopLoading());
        builder.create().show();
    }

    @Override
    public void setBackgroundImage(PhotoSize photoSize) {
        if(photoSize != null && photoSize.getSource() != null){
                Picasso.with(this).load(photoSize.getSource()).into(mBackgroundPhoto);
        }
    }

    @Override
    public void setWeather(Weather weather) {
        if(weather.getCity() != null) {
            mCityNameText.setText(weather.getCity().getName());
        }
        if(weather.getTemp() != null) {
            mWeatherTemp.setText(Float.toString(weather.getTemp()));
        }
        if(weather.getWindSpeed() != null) {
            mWindSpeedText.setText(Float.toString(weather.getWindSpeed()));
        }
        if(weather.getIconUrl() != null){
            mWeatherImage.setVisibility(View.VISIBLE);
            Picasso.with(this).load(weather.getIconUrl()).into(mWeatherImage);
        }
    }

    @Override
    public void setCityList(List<City> cityList) {
        if(arrayAdapter != null) {
            arrayAdapter.clear();
            arrayAdapter.addAll(cityList);
            arrayAdapter.notifyDataSetChanged();
        }
    }

    @Override
    public void addCityInList(City city) {
        if(arrayAdapter != null){
            arrayAdapter.add(city);
            arrayAdapter.notifyDataSetChanged();
        }
    }

    @Override
    public void hideCityList() {
        mCitySelectLayout.setVisibility(View.GONE);
    }

    @Override
    public void showCityList() {
        mCitySelectLayout.setVisibility(View.VISIBLE);
    }

    @Override
    public void showError(String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Error!!");
        builder.setMessage(message);
        builder.setPositiveButton("Yes", (dialog, which) -> mHomePresenter.stopLoading());
        builder.create().show();
    }

    @Override
    public void hideError() {

    }

    @Override
    public void showRefreshing() {
        mSwipeRefreshLayout.setRefreshing(true);
    }

    @Override
    public void hideRefreshing() {
        mSwipeRefreshLayout.setRefreshing(false);
    }

    @Override
    public void onStartLoading(){
    }

    @Override
    public void onFinishLoading() {
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case TAG_CODE_PERMISSION_LOCATION: {
                mHomePresenter.getLocation();
            }
        }
    }

    @Override
    public void onRefresh() {
        mHomePresenter.refrash();
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        mHomePresenter.loadWeather(query);
        if(searchView != null){
            searchView.setIconified(true);
            searchView.clearFocus();
        }
        return true;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        return false;
    }

    @Override
    public void onFocusChange(View v, boolean hasFocus) {
        if(hasFocus){
            showCityList();
        }else {
            hideCityList();
        }
    }
}
