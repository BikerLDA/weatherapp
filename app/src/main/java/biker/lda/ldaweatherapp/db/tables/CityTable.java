package biker.lda.ldaweatherapp.db.tables;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

public class CityTable {
    @NonNull
    public static final String TABLE = "cities";

    @NonNull
    public static final String COLUMN_ID = "_id";

    @NonNull
    public static final String COLUMN_NAME = "name";

    @Nullable
    public static final String COLUMN_LON = "lon";

    @Nullable
    public static final String COLUMN_LAT = "lat";

    // This is just class with Meta Data, we don't need instances
    private CityTable() {
        throw new IllegalStateException("No instances please");
    }

    @NonNull
    public static String getCreateTableQuery() {
        return "CREATE TABLE " + TABLE + "("
                + COLUMN_ID + " INTEGER NOT NULL PRIMARY KEY, "
                + COLUMN_NAME + " TEXT NOT NULL,"
                + COLUMN_LON + " REAL,"
                + COLUMN_LAT + " REAL"
                + ");";
    }

}
